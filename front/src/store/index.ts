import { createStore, Store } from "vuex";
import { InjectionKey } from 'vue'

const ALL_THEME = ['light', 'dark'] as const;

export type type_theme = typeof ALL_THEME[number];

export interface State {
  theme: type_theme
}

function isStringInTheme(x: string): x is type_theme {
  return ALL_THEME.includes(x as type_theme);
}

export const key: InjectionKey<Store<State>> = Symbol()

function initTheme(): type_theme {
  const save_theme = window.localStorage.getItem("theme");
  if (save_theme && isStringInTheme(save_theme)) {
    return save_theme
  }
  return new Date().getHours() >= 7 && new Date().getHours() <= 19 ? 'light' : 'dark' 
}

export default createStore<State>({
  state: {
    theme: initTheme(),
  },
  mutations: {
    changeTheme ( state: State, payload: type_theme) {
      window.localStorage.setItem("theme", payload);
      state.theme = payload;
    }
  },
  actions: {},
  modules: {},
});
