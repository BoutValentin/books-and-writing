#!/bin/sh
set -e

if [ "$NODE_ENV" != "production" ]; then    
    echo "Installing dependencies"
    npm install
fi

node index.js