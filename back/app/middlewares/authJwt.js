const jwt = require("jsonwebtoken");
const {secrets} = require("../../config/auth.config");

const abstractTokenCheck = ({req, res, next, callback}) => {
    const token = req.headers["x-access-token"];
    if (!token) {
        return res.status(403).send({message: "No token provided !"});
    }
    jwt.verify(token, secrets, (err, decoded) => {
        if (err) {
            console.log(err);
            return res.status(401).send({message: "Unauthorized !"});
        }
        req.userId = decoded.id;
        callback({req, res, next, err, decoded});
    })
}

const verifyToken = (req, res, next) => {
    const callback = ({next}) => {
        next();
    }
    abstractTokenCheck({req, res, next, callback});
}

module.exports = {
    verifyToken,
    abstractTokenCheck,
}