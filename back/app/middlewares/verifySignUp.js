const { user: User} = require('../models');

const checkDuplicateUsernameOrEmail = ({body: {username, email}}, res, next) => {
    User.findOne({$or: [ {username}, {email} ]}).exec((err, user) => {
        console.log(err, user)
        if (err) {
            res.status(500).send({message: err});
            return;
        }
        if (user) {
            res.status(400).send({message: "Failed! Username or email already user !"});
            return;
        }
        next();
    })
}

module.exports = {
    checkDuplicateUsernameOrEmail
}
