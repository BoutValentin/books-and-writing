const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/auth.controller");
const { API_BASE_ENTRYPOINT } = require("../../config/routes.config");

const API_ENTRYPOINT_ROUTE = `${API_BASE_ENTRYPOINT}/auth`

module.exports = function(app) {
    app.use((_, res, next) => {
        res.header("Access-Control-Allow-Headers", "x-access-token, Origin, Content-Type, Accept");
        next();
    })

    // TODO REMOVE ABILITIES TO SIGN UP
    app.post(`${API_ENTRYPOINT_ROUTE}/signup`, [verifySignUp.checkDuplicateUsernameOrEmail], controller.signUp);
    app.post(`${API_ENTRYPOINT_ROUTE}/signin`, controller.signIn);
    app.get(`${API_ENTRYPOINT_ROUTE}/check`, controller.check);
}