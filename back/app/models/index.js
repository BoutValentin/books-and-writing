const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {
    mongoose,
    article: require('./article.model'),
    author: require('./author.model'),
    book: require('./book.model'),
    categorie: require('./categorie.model'),
    editor: require('./editor.model'),
    image: require('./image.model'),
    link: require('./link.model'),
    user: require('./user.model'),
};

module.exports = db;
