const {model, Schema} = require('mongoose');

const editor_schema = new Schema({
    name: String,
})

const Editor = model("Editor", editor_schema);

module.exports = Editor;
