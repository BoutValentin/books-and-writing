const {model, Schema} = require('mongoose');

const categorie_schema = new Schema({
    name: String,
})

const Categorie = model("Categorie", categorie_schema);

module.exports = Categorie;
